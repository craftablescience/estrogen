# Estrogen Format Specification
`Estrogen` ( initially/also known as just `E` ) is a simple data/configuration format designed to be easy to read and write, and which maps to a dictionary.   
By ender@aurorians / ENDERZOMBI102  
Spec written By firm@aurorians / Firmament

Specification
-
- E is case-sensitive.
- An E file file must be a valid UTF-8 encoded Unicode document.
- Whitespace means either tab (0x09) or space (0x20).
- Newline means either LF (0x0A) or CRLF (0x0D 0x0A).

Comments
-
An octothorpe marks the rest of the current line as a comment.

```Estrogen
# this is a full line comment
key: value # this is a comment at the end of a line
another: "# this is still a comment, quotes do nothing in Estrogen"
escape: \# this is a string! you can escape comments
```

Control characters other than tab (such as U+0000 to U+0008, U+000A to U+001F, U+007F) are not allowed in comments.

Key-Value Pair
-
The primary building block of an Estrogen document is a pair of a key and a value.

Keys are on the left of the colon and values are on the right. Whitespace is ignored around key names and values. The key and the colon must be on the same line.

```Estrogen
key: value
```

Values must have one of the following types.

- None
- Boolean
- Integer
- Float
- String
- Array
- Dictionary

Unspecified values are invalid.

```Estrogen
key: # THIS IS INVALID
```

There must be a newline (or EOF) after a key-value pair. Putting multiple on the same line will make the value a string.
```Estrogen
first: ender last: Aurora # THIS HAS BECOME A SINGLE STRING ( `ender last: Aurora` )
```

Keys
-
A key is an unquoted string of ASCII characters until a colon, leading whitespace is ignored.

Keys may contain any character which isn't a colon (^:). Note that keys are allowed to be composed of only ASCII digits, e.g. `1234`, but are always interpreted as strings.

```Estrogen
key : value
another_key: value
another-key: value
1234 : value
```

Indentation is treated as division, like in python, it signals a scope.

Defining a key multiple times results in implementation-defined behaviour (usually an error).

```Estrogen
# DO NOT DO THIS
name: ender
name: Firmament
```

None
-
None represents a value without a particular meaning, some languages uses it to represent the absence of a value.

```Estrogen
key: none
```

Boolean
-
Booleans are just the tokens you're used to. Always lowercase.

```Estrogen
bool0: true
bool1: false
```

Integer
-
Integers are whole numbers, which may be made negative by prefixing them with a minus sign. Size is implementation-defined.

```Estrogen
int0: 42
int1: 0
int2: -17
```

Float
-
Floats should be implemented as IEEE 754 binary64 values.

A float consists of an integer part (which follows the same rules as decimal integer values) followed by a fractional part and/or an exponent part. If both a fractional part and exponent part are present, the fractional part must precede the exponent part.

```Estrogen
# fractional
float0: 3.1415
float1: -0.01

# exponent
float2: 5e+22
float3: 1e06
float4: -2E-2

# both
float5: 6.626e-34
```
A fractional part is a decimal point followed by one or more digits.

An exponent part is an E (either upper or lower case) followed by an integer part (which follows the same rules as decimal integer values but may include leading zeros).

The decimal point, if used, must be surrounded by at least one digit on each side.

```Estrogen
# INVALID FLOATS
invalid_float_0: .7
invalid_float_1: 7.
invalid_float_2: 3.e+20
```

String
-
String in Estrogen are very simple, they are unquoted values not starting with a number, and that must contain only valid UTF-8 characters.
Any Unicode character may be used except those that must be escaped: backslash and the control characters other than tab (U+0000 to U+0008, U+000A to U+001F, U+007F).

```Estrogen
str: I'm a string. "Quotes don't need escaping". You\tCan\u00E9\nUse escape codes\tSF.
```

For a matter of convenience, some characters have a more compact escape sequence.

| Escape Code | Result | Result (Unicode) |
|---|---|---|
| `\t` | tab | `U+0009` |
| `\n` | newline | `U+000A` |
| `\#` | octothorpe | `U+0023` |
| `\(` | left parenthesis | `U+0028` |
| `\)` | right parenthesis | `U+0029` |
| `\\` | backslash | `U+005C` |
| `\uXXXX` | unicode | `U+XXXX` |
| `\UXXXXXXXX` | unicode | `U+XXXXXXXX` |

Any Unicode character may be escaped with the `\uXXXX` or `\UXXXXXXXX` forms. The escape codes must be valid Unicode scalar values.

Other escape sequences are ill-formed and should be treated as such by implementations.

Type specifiers and lack of whitespace preservation can sometimes conflict with intended string values. For cases where strings are interpreted in the wrong way, wrap the string with raw format indicators `(` and `)`. Either parenthesis can be escaped if this behavior is not desired: strings are only treated as raw strings if two unescaped parenthesis bound it on the left and right.

```Estrogen
str: ( String: I am a string, yes I am   )
# interpreted as: " String: I am a string, yes I am   "
```

Raw string format indicators can also be used to create multiline strings.

```Estrogen
multiline_str: (This string
spans
multiple lines!)
# interpreted as: "This string\nspans\nmultiple lines!"
```

Array
-
Arrays are values indented by a +1 factor, of which elements are separated by newlines.
Arrays can contain the same data values allowed in key-value pairs, with some differences.

```Estrogen
integers:
	1
	2
	3
subarrays_of_ints:
	# this level of indentation is the outer array
		# this level is the inner array
		1
		2
	# empty lines marks the end of an indented block, so the inner array ends here

		3
		4
		5
nested_mixed_array:
		1
		2

		a
		b
		c
string_array:
	so
	many
	strings

# mixed-type arrays are allowed
numbers:
	0.1
	0.2
	0.5
	1
	2
	5
mixed_object_array:
	foo@example.com
	24
	# the following is a dictionary, we'll get to those
		name: Justin Tube
		email: just.intube@example.com
		url: https://example.com/justintube
object_array:
	# this is a type specifier, we'll get to those
	User:
		name: Justin Tube
		password: helloworld123
		email: just.intube@example.com
	User:
		name: Justin Time
		password: jit.arecool
		email: jit.comp@example.com
```

Dictionary
-
Dictionaries (also known as hash tables or just tables) are collections of key-value pairs.
They are defined by simply using a key plus indentation.

```Estrogen
table-0:
	key0: a string
	key1: 123

table-1:
	key0: some another string
	key1: 456

table-2:
	root: User:
		name: Justin Tube
		password: helloworld123
```

Type Specifier
-
An type specifier is an extra string value after a key. This name can be used by implementations to specify types for values, or determine the type of the
value assigned to a key.

```Estrogen
temperature: Kelvin: 120
```

For the purposes of determining the type of a standard Estrogen value assigned to a key, the following type names are reserved.

| Type Name | Data Type | Default Value |
|---|---|---|
| `None` | None | `none` |
| `Boolean` | Boolean | `false` |
| `Integer` | Integer | `0` |
| `Float` | Float | `0.0` |
| `String` | String | Empty |
| `Array` | Array | Empty |
| `Dict` | Dictionary | Empty |

An example of type specifiers with explicit values:

```Estrogen
none_type: None: none
boolean_type: Boolean: true
integer_type: Integer: 24
float_type: Float: 34.5
string_type: String: Hello World!
array_type: Array:
	1
	2
	3
dict_type: Dict:
	subkey: value
```

And with implicit values:

```Estrogen
none_type: None:       # none
boolean_type: Boolean: # false
integer_type: Integer: # 0
float_type: Float:     # 0.0
string_type: String:   # empty string
array_type: Array:     # empty array
dict_type: Dict:       # empty dictionary
```

If the value assigned to the key does not match the given type, it is ill-formed and should be treated as such by implementations.

Filename Extension
-
Estrogen files should use the extension `.e`.

MIME Type
-
When transferring Estrogen files over the internet, the appropriate MIME type is application/estrogen.
