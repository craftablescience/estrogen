# Estrogen
A simple configuration/data format

By ender, Firmament

Objectives
-
Estrogen aims to be a minimal configuration/data file format which is easy to read and write, while also having little-to-no control characters.
Estrogen was made so a document will map to a dictionary.
Estrogen should be able to be parsed easily into data structures in many languages.

Example
-
```Estrogen
# this is an Estrogen document

name: JsRefactor
id: bm.enderzombi102.jsrefactor
desc: Polyfills for BM's CEF widgets

authors:
	ENDERZOMBI102 # a list

entrypoints:
	main: self.src.main # a dictionary
	event: self.src.event

dependencies: # a list of dictionaries
	Package: # a dictionary of type `Package` 
		name: Pillow
		version: 1.7.0
		optional: false
	Plugin:
		id: bm.enderzombi102.jsengine
		url: github.com/ENDERZOMBI102/plugins$jsengine
		version: 1.0.0+build.24

provides: # a dictionary of dictionaries
    pillow: Package:
        name: Pillow
        version: 1.7.0
```

Implementations
-
N/D
